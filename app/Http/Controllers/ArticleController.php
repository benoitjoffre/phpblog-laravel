<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
class ArticleController extends Controller
{
    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('article', ['article' => $article]);
    }


    public function list()
    {
        $articles = Article::All()
        ->sortByDesc("publish_date");
        return view('articles', ['articles' =>$articles]);
    }
    
}

