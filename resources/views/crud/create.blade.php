@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
        <div class="card">
                <div class="card-header">
                  Add Article
                </div>
                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                    </div><br />
                  @endif
                    <form method="post" action="{{ route('crud.store') }}">
                        <div class="form-group">
                            @csrf
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" name="title"/>
                        </div>
                        <div class="form-group">
                            <label for="extract">Extract :</label>
                            <input type="text" class="form-control" name="extract"/>
                        </div>
                        <div class="form-group">
                              <label for="content">Content :</label>
                              <textarea class="form-control" name="content" id="content" cols="30" rows="5"></textarea>
                          </div>
                          <div class="form-group">
                                  <label for="image">Image Link :</label>
                                  <input type="text" class="form-control" name="image"/>
                              </div>
                        
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
              </div>
</div>
</div>
@endsection