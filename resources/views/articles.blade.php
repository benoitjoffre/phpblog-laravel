<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>
    <header class="header_index">
        <h3 class="title_header_index">BLOG WITH LARAVEL</h3>
        <a href="{{ route('login') }}"><input type="button" class="login_header_index" value="LOGIN"></a>  
    </header>
    <section class="container">
        <div class="card-columns">
                @foreach ( $articles as $article )
                <a href="{{route('article',['id' => $article->id])}}">
                    <div class="card">
                        <div class="card-body">
        
                                <h3 class="card-title text-center">{{$article->title}}</h3>
                                <img class="card-img-top" src="{{$article->image}}" alt="">
                                <p class="card-text text-justify">{{$article->extract}}</p>
                                <hr class="my-4">
                                <p class="card-text text-left">Published : {{$article->publish_date}}</p>
                                <footer class="blockquote-footer">
                                    <cite>{{$article->author->username}}</cite>
                                </footer>
                        </div>
                    </div>    
                </a> 
                @endforeach       
        
        </div>
    </section>
</body>
</html>