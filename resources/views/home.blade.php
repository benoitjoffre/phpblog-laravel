@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3 class="text-center">WELCOME {{ Auth::user()->username }}</h3>
                    <p class="text-center">You are logged in!</p>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
            
        </div>
    </div>
</div>

<section class="container">
        <div class="card-columns">
                @foreach ( $articles as $article )
                
                    <div class="card">
                        <div class="card-body">
        
                                <h3 class="card-title text-center">{{$article->title}}</h3>
                                <img class="card-img-top" src="{{$article->image}}" alt="">
                                <p class="card-text text-justify">{{$article->extract}}</p>
                                <hr class="my-4">
                                <p class="card-text text-left">Published : {{$article->publish_date}}</p>
                                <footer class="blockquote-footer">
                                    <cite>{{$article->author->username}}</cite>
                                </footer>
                        </div>
                    </div>    
                
                @endforeach       
        
        </div>
    </section>

@endsection
